from django.urls import path
from django.conf.urls import url


from rest_framework import routers
from .viewsets import CategoriaViewSet, ProductoViewSet
app_name = 'api_ecommerce'

router = routers.SimpleRouter()
router.register('api_categoria', CategoriaViewSet)
router.register('api_producto', ProductoViewSet)

urlpatterns = router.urls
