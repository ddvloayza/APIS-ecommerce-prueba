from django.db import models
from django.urls import reverse


class Categoria(models.Model):
    nombre_categoria = models.CharField('Nombre de Categoria', max_length=200, blank=True, null=True)
    descripccion_categoria = models.CharField('Descripccion de la Categoria', max_length=5000, blank=True, null=True)


class Producto (models.Model):
    categoria = models.ForeignKey(Categoria, related_name='categoria', blank=True, null=True, on_delete=models.CASCADE)
    nombre_producto = models.CharField('Nombre del Producto', max_length=200, blank=True, null=True)
    caracteristicas_producto = models.CharField('Caracteristicas del Producto', max_length=5000, blank=True, null=True)
    precio = models.DecimalField("Precio", max_digits=5, decimal_places=2)


class Carrito(models.Model):
    producto = models.ForeignKey(Producto, blank=True, null=True, on_delete=models.CASCADE)
    fecha_agregado = models.DateTimeField(auto_now=True)


class Pedido (models.Model):
    carrito = models.ForeignKey(Carrito, blank=True, null=True, on_delete=models.CASCADE)
    fecha_agregado = models.DateTimeField(auto_now=True)
    pagado = models.BooleanField(default=False)
    