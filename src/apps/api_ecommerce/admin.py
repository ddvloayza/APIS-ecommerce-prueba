from django.contrib import admin
from apps.api_ecommerce.models import ( Categoria, Producto, Carrito, Pedido )

@admin.register(Categoria)
class CategoriaAdmin(admin.ModelAdmin):
    pass


@admin.register(Producto)
class ProductoAdmin(admin.ModelAdmin):
    pass

