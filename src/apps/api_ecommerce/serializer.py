from rest_framework import serializers
from apps.api_ecommerce.models  import Categoria, Producto, Carrito, Pedido
from django.contrib.sites.shortcuts import get_current_site


class ProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = '__all__'


class CategoriaSerializer(serializers.ModelSerializer):
    categoria = ProductoSerializer(many=True)
    class Meta:
        model = Categoria
        fields = ('nombre_categoria', 'descripccion_categoria', 'categoria')
